FROM fedora:35

RUN dnf -y install wget wireguard-tools

RUN groupadd headscale && useradd -u 1001 -g headscale headscale

ENV HEADSCALE_VERSION=0.15.0
ENV HEADSCALE_CONFDIR=/etc/headscale
ENV HEADSCALE_LIBDIR=/var/lib/headscale

RUN cd /tmp && \
    wget \
       https://github.com/juanfont/headscale/releases/download/v${HEADSCALE_VERSION}/headscale_${HEADSCALE_VERSION}_linux_amd64 \
       -O /usr/local/bin/headscale && \
    chmod a+x /usr/local/bin/headscale && \
    mkdir -p $HEADSCALE_CONFDIR && \
    mkdir -p $HEADSCALE_LIBDIR && \
    chown headscale $HEADSCALE_CONFDIR $HEADSCALE_LIBDIR && \
    touch $HEADSCALE_LIBDIR/db.sqlite

COPY run-headscale /usr/local/bin/run-headscale
