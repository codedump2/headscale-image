#!/bin/bash

HERE=$(cd `dirname $0` && pwd)

IMAGE=${IMAGE:=registry.gitlab.com:codedump2/headscale-image}

# try "debug" ;-)
GIN_MODE=${GIN_MODE:=release}

podman run -ti --rm \
     -v $HERE/etc-headscale:/etc/headscale:z \
     -v $HERE/var-lib-headscale:/var/lib/headscale:z \
     -e SERVER_URL=https://vpn.v.rootshell.ro:443 \
     -e PREFIX_POOL="[172.16.5.0/24]" \
     -p 8080:8080/tcp \
     -p 9090:9090/tcp \
     -p 50443:50443/tcp \
     -p 3478:3478/udp \
     --name pod-headscale \
     --hostname pod-headscale \
     $IMAGE $@

#     --ip 172.16.128.48 \
